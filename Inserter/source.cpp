#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

using namespace std;




// The copying is not generic enough... in one case we need to call a push_back
// without advancing the iterator (missing `++first` statement), and in the
// other case the iterator is dereferenced ( `operator*` ), then moved forward.

// Idea:
// Code a class that _hides_ the push back call inside an overload of operator*
template <typename Container>
class BackInserterIterator{
  private:
    Container* the_container;

  public:
      explicit BackInserterIterator(Container& c) : the_container(&c) {}

      BackInserterIterator& operator= (typename Container::const_reference rhs) {
          the_container->push_back(rhs);
          return *this;
      }

      // We can at this point replace the explicit push_back (first loop below)
      // with a call to operator=.

      // We still need to
      //
      // 1. overload the dereference operator `operator*`; and
      // 2. _fake_ the forward move of the iterator.
      BackInserterIterator& operator*() {
          return *this;
      }

      BackInserterIterator& operator++() {  // Pre-fixed version
          return *this; // Notice there is no increment!!!
      }

      BackInserterIterator& operator++(int /*unused*/) {  // Post-fixed version
          return *this; // Notice there is no increment either!!!
      }

};




//This template function makes our code pretty!
template <typename Container>
inline
BackInserterIterator<Container>
our_back_inserter(Container& c) {
    return BackInserterIterator<Container>(c);
}



int main() {

    int a[] = { 4, 6, 2, 8, 1, 4, 2, 0, 9 };

    // We can copy an array into a vector...
    vector<int> v;
    // v.resize(9);   // Oops. The vector does not have room to hold the array.



    // copy(std::begin(a), std::end(a), back_inserter(v));
    //
    // No big deal, right! We could have done it ourselves...
    int* first = a;
    int* last = a + 9;
//    BackInserterIterator< vector<int> > target(v);
    auto target = our_back_inserter(v);

    while (first != last) {
//        v.push_back(*first);   // <-- Is replaced with _something_ = *first;
        *target = *first;
        ++first;
        ++target;
    }


    // We can then copy the vector to std::cout
    cout << "v: ";
    copy(std::begin(v), std::end(v), ostream_iterator<int>(cout, " "));
    cout << '\n';



    // and even reverse-copy the array to std::cout
    cout << "a: ";
    auto rfirst = std::rbegin(a);
    auto rlast = std::rend(a);
    ostream_iterator<int> destination(cout, "-");
    while (rfirst != rlast) {
        *destination = *rfirst;
        ++rfirst;
        ++destination;
    }
    cout << '\n';



    // The "problem"... What problem???

    return 0;

}
